﻿using UnityEngine;

public class GenericItemPickup : MonoBehaviour
{
    [Header("Parameters")]
    [Tooltip("Item to pickup")]
    public Item item;	// Item to put in the inventory if picked up

    // optimizar codigos duplicados especificando en el item que clase es
    

    Pickup m_Pickup;

    private void Start()
    {
        m_Pickup = GetComponent<Pickup>();
        DebugUtility.HandleErrorIfNullGetComponent<Pickup, HealthPickup>(m_Pickup, this, gameObject);

        // Subscribe to pickup action
        m_Pickup.onPick += OnPicked;
    }

    void OnPicked(PlayerCharacterController player)
    {
        m_Pickup.PlayPickupFeedback();

        Inventory.instance.Add(item);	// Add to inventory

        if (item.itemType == ItemType.WEAPON)
        {
            item.Use();
        }
        Destroy(gameObject);
    }
}
