﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * primero comprobar que el sistema ya funciona 
 * agregando objetos que no hacen nada al inventario y usandolos
 * 
 * adaptar los antiguos itempickup
 * los consumibles por una parte, con un nuevo codigo anclado al inventario
 * 
 * las armas por otro lado, respetando el anterior sistema de control de armas
 * pero añadiendo representaciones tambien en el inventario, que solo seran cosmeticas
 * para permitir ver que armas tiene el jugador, pero para cambiar entre ellas tiene que hacerlo
 * como hasta ahora
 * 
 */
public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public Action onItemChangedCallback;

    public int space = 10;  // Amount of item spaces

    // Our current list of items in the inventory
    public List<Item> items = new List<Item>();
    
    // Add a new item if enough room
    public void Add(Item item)
    {
        if (item.showInInventory)
        {
            if (items.Count >= space)
            {
                Debug.Log("Not enough room.");
                return;
            }

            items.Add(item);

            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }
    }

    // Remove an item
    public void Remove(Item item)
    {
        items.Remove(item);

        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }
}
