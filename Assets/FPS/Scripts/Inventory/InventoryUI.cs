﻿using UnityEngine;
using UnityEngine.EventSystems;
/* This object manages the inventory UI. */

public class InventoryUI : MonoBehaviour
{

    #region Singleton

    public static InventoryUI instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject inventoryUI;  // The entire UI
    public Transform itemsParent;   // The parent object of all the items

    [Tooltip("Master volume when menu is open")]
    [Range(0.001f, 1f)]
    public float volumeWhenMenuOpen = 0.5f;

    Inventory inventory;    // Our current inventory

    void Start()
    {
        inventory = Inventory.instance;
        inventory.onItemChangedCallback += UpdateUI;
    }

    // Check to see if we should open/close the inventory
    void Update()
    {
        if (InGameMenuManager.instance.menuRoot.activeSelf)
        {
            return;
        }

        // Lock cursor when clicking outside of menu
        if (!inventoryUI.activeSelf && Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (Input.GetKeyDown(KeyCode.I) || (inventoryUI.activeSelf && Input.GetKeyDown(KeyCode.Escape)))
        {
            SetMenuActivation(!inventoryUI.activeSelf);
            UpdateUI();
        }
    }

    void SetMenuActivation(bool active)
    {
        inventoryUI.SetActive(active);

        if (inventoryUI.activeSelf)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0f;
            AudioUtility.SetMasterVolume(volumeWhenMenuOpen);

            EventSystem.current.SetSelectedGameObject(null);
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1f;
            AudioUtility.SetMasterVolume(1);
        }
    }

    // Update the inventory UI by:
    //		- Adding items
    //		- Clearing empty slots
    // This is called using a delegate on the Inventory.
    public void UpdateUI()
    {
        InventorySlot[] slots = GetComponentsInChildren<InventorySlot>();

        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }

}