﻿using UnityEngine;
/* An Item that can be consumed. */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Consumable")]
public class GenericConsumableItem : Item
{
    // This is called when pressed in the inventory
    public override void Use()
    {
        // Do something


        Debug.Log(name + " consumed!");

        RemoveFromInventory();  // Remove the item after use
    }
}
