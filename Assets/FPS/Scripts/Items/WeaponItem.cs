﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Weapon")]
public class WeaponItem : Item
{
    [Tooltip("The prefab for the weapon that will be added to the player on pickup")]
    public WeaponController weaponPrefab;

    public override void Use()
    {
        if (PlayerWeaponsManager.instance.AddWeapon(weaponPrefab))
        {
            // Handle auto-switching to weapon if no weapons currently
            if (PlayerWeaponsManager.instance.GetActiveWeapon() == null)
            {
                PlayerWeaponsManager.instance.SwitchWeapon(true);
            }
        }
    }
}
